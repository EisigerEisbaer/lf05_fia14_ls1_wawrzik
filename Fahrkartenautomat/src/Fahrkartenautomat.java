﻿import java.util.Scanner;

class Fahrkartenautomat
{
	  public static double fahrkartenbestellungErfassen() {
   	   int anzahl;
   	   double tarif; 
   	   Scanner tastatur = new Scanner(System.in);
   	   System.out.print("Wählen Sie bitte den gewünschten Tarif mit den  Ziffern 1-3 aus."
   	   		+ "Einzelfahrschein Regeltarif AB = 2,90 (1), Tageskarte Regeltarif AB= 8,60 (2), Kleingruppen-Tageskarte AB = 23,50 (3) ");
          tarif = tastatur.nextDouble();
          if (tarif == 1)
          {
        	  tarif = 2.90;
        	  
          }
          else if(tarif == 2)
           {
        	   tarif = 8.60;
           }
           else if  (tarif == 3)
          {
        	  tarif = 23.50;
          }
          System.out.print("\nAnzahl der Tickets: ");
          anzahl = tastatur.nextInt();
          if (anzahl < 1 || anzahl > 10)
          {
        	  System.out.println("Bitte beim nächsten Kauf eine gültige Ticketanzahl eingeben!");
        	  anzahl = 1;
          }
          tarif = tarif * anzahl;
          return tarif;
	  }
          
          public static double fahrkartenBezahlen(double tarif) {
        	  Scanner tastatur = new Scanner(System.in);        	  
        	  double eingezahlterGesamtbetrag;
              double eingeworfeneMünze;
              eingezahlterGesamtbetrag = 0.00;
              while(eingezahlterGesamtbetrag < tarif)
              {
           	   double a = (tarif - eingezahlterGesamtbetrag);
           	   System.out.printf("Noch zu zahlen : %.2f %.2s" ,a, "€");
           	   System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
           	   
           	   eingeworfeneMünze = tastatur.nextDouble();
                  eingezahlterGesamtbetrag += eingeworfeneMünze;
                  
              }
              return eingezahlterGesamtbetrag;
          }
          
          public static void fahrkartenAusgeben() {
        	  System.out.println("\nFahrschein wird ausgegeben");
        	  int millisekunde = 90;
              for (int i = 0; i < 8; i++)
              {
                 System.out.print("=");
                 
                warte(millisekunde);
       	
              }
              System.out.println("\n\n");
             
          }
      public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double tarif) {
    	  double rückgabebetrag;
    	  rückgabebetrag = eingezahlterGesamtbetrag - tarif;
          if(rückgabebetrag > 0.00)
          {
       	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
       	   System.out.println("wird in folgenden Münzen ausgezahlt:");

              while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
              {
           	  System.out.println("2 EURO");
   	          rückgabebetrag -= 2.00;
              }
              while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
              {
           	  System.out.println("1 EURO");
   	          rückgabebetrag -= 1.00;
              }
              while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
              {
           	  System.out.println("50 CENT");
   	          rückgabebetrag -= 0.50;
              }
              while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
              {
           	  System.out.println("20 CENT");
    	          rückgabebetrag -= 0.20;
              }
              while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
              {
           	  System.out.println("10 CENT");
   	          rückgabebetrag -= 0.10;
              }
              while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
              {
           	  System.out.println("5 CENT");
    	          rückgabebetrag -= 0.05;
              }
          }
          		
          System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                             "vor Fahrtantritt entwerten zu lassen!\n"+
                             "Wir wünschen Ihnen eine gute Fahrt.");
    	  
      }
      
      public static void warte(int millisekunde) {
 			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
      }
	
    public static void main(String[] args)
    {
      
      double tarif;
      //ich bin verwirrt
       
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       while(true) {
tarif = fahrkartenbestellungErfassen();
eingezahlterGesamtbetrag = fahrkartenBezahlen(tarif);
fahrkartenAusgeben();
rueckgeldAusgeben(eingezahlterGesamtbetrag, tarif );
      
       }
       
     
       
       
    
      
       
    }
}

public class HelloWorld {

	public static void main(String[] args) {
																							// Aufgabe 1.1
		System.out.print("Guten Tag Bryan Wawrzik!");
		System.out.println("Wilkommen in der Veranstaltung Strukturierte Programmierung.");
																						    // Aufgabe 1.2
		System.out.print("\"Hallo, wie geht es ihnen?\" ");
																							// Aufgabe 1.3
		int alter = 20;
		int groesse =  176;
		String name = "Bryan";
		System.out.println("\nIch hei�e " + name + " bin " + alter + " Jahre alt und bin " + groesse + " cm gro�. ");
		// Println funktioniert nur in einer Zeile, wo print keinen Halt vor Zeilenumbr�che macht.
																							// Aufgabe 2
		System.out.println("      *      ");
		System.out.println("     ***     ");
		System.out.println("    *****    ");
		System.out.println("   *******   ");
		System.out.println(" *********** ");
		System.out.println("*************");
		System.out.println("     ***     ");
		System.out.println("     ***     ");
																							// Aufgabe 3
		double a = 22.4234234;
		double b = 1;
	}
	
}


public class ErsteKlasse {

	public static void main(String[] args) {
																							// Aufgabe 1.1
		System.out.print("Guten Tag Bryan Wawrzik!");
		System.out.println("Wilkommen in der Veranstaltung Strukturierte Programmierung.");
																						    // Aufgabe 1.2
		System.out.print("\"Hallo, wie geht es ihnen?\" ");
																							// Aufgabe 1.3
		int alter = 20;
		int groesse =  176;
		String name = "Bryan";
		System.out.println("\nIch hei�e " + name + " bin " + alter + " Jahre alt und bin " + groesse + " cm gro�. ");
		// Println funktioniert nur in einer Zeile, wo print keinen Halt vor Zeilenumbr�che macht.
																							// Aufgabe 2
		System.out.println("      *        ");
		System.out.println("     ***       ");
		System.out.println("    *****      ");
		System.out.println("   *******     ");
		System.out.println("  *********    ");
		System.out.println(" ***********   ");
		System.out.println("*************  ");
		System.out.println("     ***       ");
		System.out.println("     ***\n     ");
																							// Aufgabe 3
		double a =  22.4234234;
		double b =    111.2222;
		double c =         4.0;
		double d = 1000000.551;
		double e =       97.34;
		
		System.out.printf(  "     %.2f\n"  , a );
		System.out.printf(  "    %.2f\n"   , b );
		System.out.printf(  "      %.2f\n" , c );
		System.out.printf(       "%.2f\n"  , d );
		System.out.printf(  "     %.2f\n"  , e );

																							// �bung mit Formatierung
		
		String s = "*************"; 
		System.out.printf("\n%8.1s  "  , s );
		System.out.printf("\n%9.3s  "  , s );
		System.out.printf("\n%10.5s "  , s );
		System.out.printf("\n%11.7s "  , s );
		System.out.printf("\n%12.9s "  , s );
		System.out.printf("\n%13.11s"  , s );
		System.out.printf("\n%14.13s"  , s );
		System.out.printf("\n%9.3s  "  , s );
		System.out.printf("\n%9.3s  "  , s );
		
	}
	
}
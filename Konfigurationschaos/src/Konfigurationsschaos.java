
public class Konfigurationsschaos {

	public static void main(String[] args) {
		String typ = "Automat AVR";
		String bezeichnung = "Q2021_FAB_A";
		String name;
		name = typ + " " + bezeichnung;
		System.out.println("Name: " + name);
		char sprachModul = 'd';
		System.out.println("Sprache: " + sprachModul);
		final byte PRUEFNR = 4; 
		System.out.println("Pruefnummer : " + PRUEFNR);
		double patrone = 46.24;
		double maximum = 100.00;
		double prozent;
		prozent = maximum - patrone;
		System.out.println("Fuellstand Patrone: " + prozent + " %");
		int muenzenCent = 1280;
		int muenzenEuro = 130;
		int summe; 
		summe = muenzenCent + muenzenEuro * 100;
		int euro;	
		euro = summe / 100;
		int cent;
		cent = summe % 100;
		System.out.println("Summe Euro: " + euro +  " Euro");
		System.out.println("Summe Rest: " + cent +  " Cent");
		boolean status;
		status = (euro <= 150) && (prozent >= 50.00) &&  (!(PRUEFNR == 5 || PRUEFNR == 6)) && (euro >= 50) && (cent != 0) && (sprachModul == 'd');
		System.out.println("Status: " + status);
	}

}
